//: # Intro a Swift

import Foundation

//: ## Variables y Constantes
var size : Double = 42.0
var answer = 42.0
size = size + 4
let name = "Anakin"

// Por defecto, usaremos siempre let.

//: ## Todo quisqui es un objeto
Int.max
Double.abs(-45)

//: ## Conversiones se hacen mediane inicializadores
let a = Int(size)

//: ## typealias: Darle un nombre normal a un tipo con nombre raro
//: Importante de cara al futuro.
typealias Integer = Int

let a1 : Integer = 99

//: ## Colecciones
// La cadena
var swift = "Nuevo lenguaje de "
swift = swift + "!"

// Arrays
let words = ["uno", "dos", "tres", "cuatro"]
words[0]

// Dictionaries
// [ clave : valor]
let numberNames = [1: "one", 2 : "two"]
numberNames[2]

//: ## Iterar
var total = ""
for element in [1,2,3,4,5,5,6,6]{
    total = "\(total) \(element + 1)"
}
// Lo más importante, es que para que la interpolación
// de cadenas funcione, el tipo ha de implementar un protocolo.
// Swift es un lenguaje creado a base de protocolos.

for (key, value) in numberNames{
    print("\(key) -- \(value)")
}


//: ## Tuplas : forma sencilla de asociar datos
let triplet = (1, "one", "Spanish")
triplet.0
triplet.1


//: ## Funciones
func h(perico a: Int, deLosPalotes b: Int) -> Int{
    return (a + b) * a
}

h(perico: 3, deLosPalotes: 5)

// Función sin nombre externo:
// la variable anónima _
func f(_ a: Int, _ b: Int) -> Int{
    return a + b
}

f(8,9)

// Ejemplo cuya llamada aporta mucha información,
// como los mensajes de Objective C
func sum(_ a: Int, _ b: Int, thenMultiplyBy c: Int) -> Int{
    return (a + b) * c
}

sum(3,4, thenMultiplyBy: 8)

// Valores por defecto
func addSuffixTo(_ a: String, suffix: String = "ingly") ->String{
    return a + suffix
}

addSuffixTo("accord")
addSuffixTo("Objective-", suffix: "C")

// Valores de retorno
func namesOfNumbers(_ a: Int) -> (Int, String, String){
    var val : (Int, String, String)
    
    switch a {
    case 1:
        val = (1, "one", "uno")
    case 2:
        val = (2, "two", "dos")
    default:
        val = (a, "got check Google translator", "vete a google")
    }
    
    return val
}

let r = namesOfNumbers(45)
let (number, english, spanish) = namesOfNumbers(2)
print(number, english, spanish)

let (_, en, es) = namesOfNumbers(1)


//: Funciones de alto nivel
typealias IntToIntFunc = (Int)->Int
let z : IntToIntFunc

// Funciones como parámetros
func apply(f: IntToIntFunc, n: Int) -> Int{
    return f(n)
}

func doubler(a:Int) -> Int{
    return a * 2
}

func add42(a: Int) -> Int{
    return a + 42
}

apply(f: doubler, n: 5)

// Función que devuelve otra función
func compose(_ f: @escaping IntToIntFunc,
             _ h: @escaping IntToIntFunc) -> IntToIntFunc{
    
    // funciones dentro de funciones??? Sip!
    func comp(_ a:Int)->Int{
        return f(h(a))
    }
    
    return comp
}

compose(doubler, add42)(8)

//: ## Sintaxis de Clausura (queda para después)


//: ## Optionals: son cajas

// Meto dentro de la caja
var maybeString: String? = "Estoy encerrado en una cajarl!"
var maybeInt : Int?

// El operador ?? permete dar un valor por defecto, cuando
// el opcional está vacio
print(maybeString ?? "No hay nada")
print(maybeInt ?? "Soy un opcional vacio")

// desempaquetado seguro
if let certainlyAString = maybeString{
    // Seguro, seguro que hay un valor
    
    print("ya te decía yo que era una cadena, joé")
    print(certainlyAString)
    
}

// Otra forma de desempaquetado seguro es el guard
// la veremos luego

// Desempaquetado inseguro
var alláVoy = maybeString!
//var decabeza = maybeInt!  // se cae la app

// Otra forma de desempaquetado seguro: optional chaining
// Lo veremos luego

//: ## Aggregate types: enums, structs, classes, tuples

enum LightSabreColor{
    case Blue, Red, Green, Purple
}

struct LightSabre{
    
    // Static or "class" property
    static let quote = "An elegant weapon for a more civilized time"
    
    // Instance properties
    var color : LightSabreColor = .Green
    var isDoubleEdged = false
    
}

class Jedi{
    
    var lightSabre = LightSabre()
    var name : String
    
    // Inicializadores
    init(name: String, lightSabre: LightSabre){
        self.name = name
        self.lightSabre = lightSabre
    }
}






